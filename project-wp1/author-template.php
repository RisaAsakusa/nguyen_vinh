<?php get_header(); ?>
<!-- Page Content -->
<div class="container">
  <div class="row">

    <!-- Post Content Column -->
    <div class="col-lg-8">

      <!-- Title -->
      <h1 class="mt-4"><?php the_title(); ?></h1>
      <!-- Author -->
      <p class="lead">
        by
        <a href="#"><?php the_author(); ?></a>
      </p>
      <hr>
      <!-- Date/Time -->
      <p>Posted on <?php the_date('F j, Y') ?> at <?php echo the_time(); ?></p>
      <hr>
      <!-- Preview Image -->
      <img class="img-fluid rounded" src="http://placehold.it/900x300" alt="">
      <hr>

      <!-- Post Content -->
      <?php the_content(); ?>
      <hr>

      <?php comments_template(); ?>
    </div>
    <!-- Sidebar Widgets Column -->
    <?php get_sidebar(); ?>
<?php get_footer(); ?>
