<!-- Search Widget -->
<div class="card my-4">
  <h5 class="card-header">Search</h5>
  <div class="card-body">

    <form action="<?php echo esc_url(home_url()); ?>" method="GET" role="form">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for..." name="s" id="s">
        <span class="input-group-btn">
          <button class="btn btn-secondary" type="submit">Go!</button>
        </span>
      </div>
     </form>

  </div>
</div>