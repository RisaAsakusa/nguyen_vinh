  <?php $args = array(
    'title_reply' => 'Leave a Comment:',
    'label_submit' => 'Submit',
  );

  comment_form( $args ); ?>

  <div id="comment-area">

  <?php if( have_comments() ):?>

  <div class="media mb-4">

    <div class="media-body">
      <ul class=" rounded-circle">

        <?php wp_list_comments(); ?>

      </ul>
    </div>
  </div>
  <!--  -->
  <?php endif; ?>
</div>

