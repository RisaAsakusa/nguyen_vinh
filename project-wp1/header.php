<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- get name, title of company -->
    <title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo esc_url( get_template_directory_uri()); ?>//vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo esc_url( get_template_directory_uri()); ?>//css/blog-home.css" rel="stylesheet">

    <?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>
    <?php wp_head(); ?>

  </head>
  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Get menu header -->
        <?php wp_nav_menu(
          array(
              'theme_location' => 'header-menu',
              'container' => 'false',
              'menu_id' => 'navbarResponsive',
              'menu_class' => 'menu navbar-collapse collapse'
           )
        ); ?>
        <!--  navigation ---------- -->
      </div>
    </nav>