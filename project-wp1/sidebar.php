<!-- Sidebar Widgets Column -->
<div class="col-md-4">

<?php get_search_form(); ?>

  <!-- Categories Widget -->
  <div class="card my-4">
    <h5 class="card-header">Categories</h5>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-6">
          <ul class="list-unstyled mb-0">

          <?php
            $args = array(
                'child_of'  => 0,
                'orderby'   => 'id',
            );
            $categories = get_categories( $args );

            foreach ( $categories as $category ) { ?>
              <li>
                <a href="<?php echo get_term_link($category->slug, 'category'); ?>"><?php echo $category->name ; ?></a>
              </li>
          <?php } ?>

          </ul>
        </div>
      </div>
    </div>
  </div>

  <!-- Side Widget -->
  <div class="card my-4">
    <h5 class="card-header">Follow us below &darr;</h5>
    <div class="card-body">
      <div class="fb-page" data-href="https://www.facebook.com/OurVietnam/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/OurVietnam/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/OurVietnam/">Tôi ♥ Việt Nam | I ♥ Vietnam</a></blockquote></div>
    </div>
  </div>
</div>