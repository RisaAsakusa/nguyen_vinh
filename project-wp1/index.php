<?php get_header(); ?>

  <!-- Support thumbnail wordpress -->

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <!-- Blog Entries Column -->

      <div class="col-md-8">
        <h1 class="my-4"><?php bloginfo('name') ?>
          <small><?php bloginfo('description') ?></small>
        </h1>

        <!-- Loop wp -->
        <?php
        if (have_posts()) :
          while (have_posts()) : the_post(); ?>

        <!-- Blog Post -->
        <div class="card mb-4">
          <div class="card-img-top">
            <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
          </div>

          <div class="card-body">

            <h2 class="card-title"><?php the_title(); ?></h2>
            <p class="card-text"><?php the_excerpt(); ?></p>
            <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More &rarr;</a>

          </div>
          <div class="card-footer text-muted">

            Posted on <?php echo get_the_date('F j, Y'); ?>, by
            <a href="<?php get_the_author_link(); ?>"><?php the_author(); ?></a>

          </div>
        </div>

        <?php endwhile;
        else :
        endif; ?>
        <!-- End-Loop wp -->

        <!-- Pagination -->
        <?php if ( $wp_query -> max_num_pages > 1 ) : ?>
        <ul class="pagination justify-content-center mb-4">
          <li class="page-item">
            <?php previous_posts_link('&larr; Older'); ?>
          </li>
          <li class="page-item">
            <?php next_posts_link('Newer &rarr;'); ?>
          </li>
        </ul>
        <?php endif; ?>
        <!-- /Pagination -->
      </div>

      <?php get_sidebar(); ?>
<?php get_footer(); ?>