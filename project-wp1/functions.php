<?php
  function register_my_menu() {
    register_nav_menu('header-menu',('Menu chính'));
    register_nav_menu('footer-navi', 'フッターのナビゲーション');
  }
  add_action( 'init', 'register_my_menu' );

  register_sidebar( array(
    'name' => 'sidebar-1',
    'id' => 'sidebar-1',
    'description' => 'サイドバーのウィジットエリアです。',
    'before_widget' => '<div id="%1$s" class="widget    %2$s">',
    'after_widget' => '</div>',
  ));

  // カスタムメニューを有効化
  add_theme_support( 'menus' );
  add_theme_support( "title-tag" );
  // カスタムメニューの「場所」を設定するコード
  register_nav_menu( 'header-navi', 'ヘッダーのナビゲーション' );

  // add post thumbnails wordpress
  add_theme_support( 'post-thumbnails' );

  add_image_size( 'post-thumbnail', 750, 300);
?>