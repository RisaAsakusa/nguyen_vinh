      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y') ?></p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo esc_url( get_template_directory_uri()); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri()); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <?php wp_footer(); ?>
  </body>
</html>

  <!-- Plugin Facebook -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>