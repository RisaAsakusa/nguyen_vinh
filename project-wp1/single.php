<?php get_header(); ?>

<!-- Page Content -->
  <div class="container">

    <div class="row">

    <!-- Post Content Column -->
    <div class="col-lg-8">

      <?php if(have_posts()) :
        while (have_posts()) : the_post(); ?>

      <!-- Title -->
      <h1 class="mt-4"><?php the_title(); ?></h1>

      <!-- Author -->
      <p class="lead">
        by
        <a href="#"><?php the_author(); ?></a>
      </p>

      <hr>

      <!-- Date/Time -->
      <p>Posted on <?php the_date('F j, Y'); ?>  at <?php echo the_time(); ?></p>

      <!--   <?php the_tags('Tag : ',', '); ?> -->
      <hr>

      <!-- Preview Image -->
      <div class="card-img-top">
        <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
      </div>
      <hr>

      <!-- Post Content -->
      <?php the_content(); ?>
      <hr>
      <?php endwhile;
        endif; ?>

      <?php comments_template(); ?>
    </div>

    <!-- Sidebar Widgets Column -->
    <?php get_sidebar(); ?>
<?php get_footer(); ?>