<?php get_header(); ?>

    <!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Blog Entries Column -->
      <div class="col-md-8">
        <h1 class="my-4">Category</h1>

        <!-- Loop wp -->
        <?php if (have_posts()) :
          while (have_posts()) : the_post(); ?>

        <!-- Blog Post -->
        <div class="card mb-4">

          <div class="card-img-top">
            <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
          </div>

          <div class="card-body">

            <h4 class="card-title"><?php the_title(); ?></h4>

            <p class="card-text"><?php the_excerpt(); ?></p>

            <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More &rarr;</a>
          </div>

          <div class="card-footer text-muted">
            Posted on <?php echo get_the_date('M-d, Y') ?> by
            <a href="<?php echo get_the_author_link(); ?>"><?php the_author(); ?></a> <br>

            <span>Categories: <?php the_category(', '); ?></span>
            <?php the_tags(', Tag : ',', '); ?>
          </div>
        </div>

        <?php endwhile;
        else : ?>
          <h1 class="my-4 text-danger">No Results</h1>
        <?php endif; ?>
        <!-- End-Loop wp -->
      </div>

      <?php get_sidebar(); ?>
<?php get_footer(); ?>